# Bioline scraper #

This README describes how to work with the project.

### About ###
Project use [Scrapy](https://scrapy.org/) as the main framework so all documentation about executing, setting and other things can be found [here](https://doc.scrapy.org/en/latest/).

### Code ###
Project contain the only one [spider](https://doc.scrapy.org/en/latest/topics/spiders.html) with name `issue` which contain a couple of methods to extract common data from the page with a list of articles and data from the page with a concrete article. During the text processing, all text formatting is erased.

### Extracting data from pdf files ###
Project use [PDFMiner](http://www.unixuser.org/~euske/python/pdfminer/index.html) for extracting data from pdf files. In case when some article has no full-text pdf file it results will be written `"Enable to find article pdf file on {response.url}"` in `full_text` value. During the text processing, all text formatting in PDF file is erased (fetching raw text data).

### Work on server ###
Scrapy is executed every 10 minutes by cron. In the current configuration, the fetched data is stored in `/opt/bioline/out.jl` in JSON lines format. There are need to create particular shell script because scrapy should be executed from the project directory.

#### crone ####
	*/10 * * * * /root/run_scrapy_for_bioline.sh

#### run_scrapy_for_bioline.sh script ####
	#!/bin/bash
	
	cd /opt/bioline/
	/usr/local/bin/scrapy crawl -o out.jl --logfile=/var/log/bioline.log --loglevel=INFO issue