import cStringIO
import requests
from scrapy import Spider, Request
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfpage import PDFPage
from pdfminer.converter import TextConverter
from pdfminer.pdfparser import PDFSyntaxError


class IssueSpider(Spider):
    name = "issue"
    start_urls = [
        'http://bioline.org.br/titles?id=md&year=2016&vol=19&num=01&keys=V19N1'
    ]

    def __init__(self, debug=False):
        super(IssueSpider, self).__init__()
        self._debug = debug

    def parse(self, response):
        context = {
            'journal_name': self.extract_from_page(
                response,
                'font[@class="paperTitle"]'),
            'journal_issn': self.extract_from_page(
                response,
                'font[@class="paperISSN"]'),
            'authors_affiliations': self.extract_from_page(
                response,
                'font[@class="paperPublisher"]'),
        }
        for article_url in response.xpath('//form//ul//a/@href').extract():
            yield Request(response.urljoin(article_url),
                          meta={'context': context},
                          callback=self.parse_article)

    def parse_article(self, response):
        pdf_url = response.xpath('//td[@align="right" and @id="bottomLine"]'
                                 '/a/@href').extract_first()
        if 'pdf' in pdf_url:
            try:
                full_text = self.fetch_full_text_from_pdf(
                    response.urljoin(pdf_url))
            except PDFSyntaxError:
                return "PDFSyntaxError occurred during pdf processing. " \
                       "Article url: %s" % response.url
        else:
            full_text = 'Enable to find article pdf file on %s' % response.url
        data = {
            'title': self.extract_from_page(response,
                                            'font[@class="AbstractTitle"]'),
            'author': self.extract_from_page(response,
                                             'font[@class="AbstractAuthor"]'),
            'abstract': self.extract_from_page(response,
                                               'div[@class="AbstractText"]'),
            'full_text': full_text,
        }

        if self._debug:
            data['url'] = response.url

        data.update(response.meta['context'])
        return data

    @staticmethod
    def fetch_full_text_from_pdf(url):
        response = requests.get(url, stream=True)

        string_io = cStringIO.StringIO()
        res_man = PDFResourceManager(caching=True)
        device = TextConverter(res_man, string_io)
        interpreter = PDFPageInterpreter(res_man, device)

        for page in PDFPage.get_pages(cStringIO.StringIO(response.content), check_extractable=True):
            interpreter.process_page(page)
        return string_io.getvalue()

    @staticmethod
    def extract_from_page(response, query):
        text_fragments = response.xpath(
            '//%s/descendant-or-self::*/text()' % query).extract()
        striped_fragments = map(lambda i: i.strip(), text_fragments)
        pure_fragments = filter(lambda i: i, striped_fragments)
        return ' '.join(pure_fragments)
